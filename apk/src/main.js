import Vue from 'vue'
import App from './App'
import uView from 'uview-ui'
Vue.config.productionTip = false

App.mpType = 'app'

const config = require('@/api/config')
uni.$u.http.setConfig(() => {
    return config
})
Vue.use(uView)
// 如此配置即可
uni.$u.config.unit = 'rpx'
const app = new Vue({
  ...App
})
app.$mount()
